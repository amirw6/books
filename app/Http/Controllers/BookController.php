<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Contracts\Validation\ValidationException;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $id=Auth::id();
        $books = Book::all();
        return view('books.index', ['books'=>$books]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   if (Gate::denies('manager')){
        abort(403,"Sorry you are not allowed to create new Book");
    }
        return view('books.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   

        $id=Auth::id();
        $book = new Book();
        $title = $book->title;
        
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:20'
        ]);

        if ($validator->passes())
        {
        $book->title = $request->title;
        $book->author = $request->author;
        $book->user_id = $id;
        $book->save();
        return redirect('books');    
        }
        else{
            $errors = $validator->messages();
            return view('books.create',['errors'=>$errors]);
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book =Book::find($id);
        return view('books.edit', compact('book'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $book = Book::find($id);
        if (Gate::denies('manager')){
            if($book->status==0){
        if(!$book->user->id == Auth::id()) return(redirect('books')); 
        $book -> update($request->except(['_token']));
        if($request->ajax()){
            return Response::json(array('result'=>'success', 'status'=>$request->status),200);
        }
    }
        return redirect('books');
    
    }
        if(!$book->user->id == Auth::id()) return(redirect('books')); 
        $book -> update($request->except(['_token']));
        if($request->ajax()){
            return Response::json(array('result'=>'success', 'status'=>$request->status),200);
        }
        return redirect('books');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::find($id);
        $book->delete();
        return redirect('books');

    }
}
