<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Notifiable;

class Book extends Model
{
    
    protected $fillable = [
        'title','author','status'
    ];


    public function user(){
        return $this->belongsTo('App\User');
    }




}
