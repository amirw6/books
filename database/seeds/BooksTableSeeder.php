<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert(
[
	[
            'title' => 'Fast and Black',
            'author' => 'yoyo',
            'user_id' => 1,
            'created_at' => date('Y-m-d G:i:s'),
	],
	[
        'title' => 'Sex in the big city',
        'author' => 'Jessica',
        'user_id' => 1,
        'created_at' => date('Y-m-d G:i:s'),
    ],

    [
    'title' => 'Chipopo in africa',
    'author' => 'zion',
    'user_id' => 1,
    'created_at' => date('Y-m-d G:i:s'),
    ],
    [   
    'title' => 'Mulan',
    'author' => 'Musho',
    'user_id' => 1,
    'created_at' => date('Y-m-d G:i:s'),
    ],
    [
    'title' => 'Shan Yu',
    'author' => 'Mungolia',
    'user_id' => 1,
    'created_at' => date('Y-m-d G:i:s'),
    ],
]);
    }

}
