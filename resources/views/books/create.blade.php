
@extends('layouts.app')

@section('content')


<h1>Create New Book</h1>
<form method = 'post' action="{{action('BookController@store')}}">
{{csrf_field()}}

<div class = "form-group">

    <label for = "title">Name of Book:</label>
    <input type= "text" class = "form-control" name= "title">
    <br>
        @foreach($errors->all() as $error)
        <li>{{$error}}</li>
        @endforeach

</div>
<br> 
<div class = "form-group">
    <label for = "author">Name of Author:</label>
    <input type= "text" class = "form-control" name= "author">
</div>


<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Save">
</div>

</form>
@endsection