<h1>Edit  Book</h1>
<form method = 'post' action="{{action('BookController@update',$book->id)}}">
@csrf
@method('PATCH')
<div class = "form-group">
    <label for = "title">Name of Book:</label>
    <input type= "text" class = "form-control" name= "title" value = "{{$book->title}}">
</div>
<br> 
<div class = "form-group">
    <label for = "author">Name of Author:</label>
    <input type= "text" class = "form-control" name= "author" value = "{{$book->author}}">
</div>

@extends('layouts.app')

@section('content')

<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Save">
</div>

</form>


<form method = 'post' action="{{action('BookController@destroy', $book->id)}}">
@csrf
@method('DELETE')
<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Delete book">
</div>

</form>


@endsection