
@extends('layouts.app')

@section('content')

<h1>This is your book list</h1>
<table>
  <tr>
    
    <th>Title</th>
    <th>Author</th>
    <th>Already read</th>
  </tr>

    @foreach($books as $book)
    <tr>

       
      <td><a href= "{{route('books.edit', $book->id )}}"> {{$book->title}} </a></td>
      <td><a href= "{{route('books.edit', $book->id )}}"> {{$book->author}} </a></td>
      <td>@if ($book->status)
           <input type = 'checkbox' id ="{{$book->id}}" checked>
           
       @else
           <input type = 'checkbox' id ="{{$book->id}}">
       @endif</td>
    </tr>
    @endforeach

</table>

<a href="{{route('books.create')}}">Create New todo </a>

<script>
       $(document).ready(function(){
           $(":checkbox").click(function(event){
               $.ajax({
                   url: "{{url('books')}}" + '/' + event.target.id ,
                   dataType:'json' ,
                   type:'put',
                   contentType:'application/json',
                   data: JSON.stringify({'status':event.target.checked, _token:'{{csrf_token()}}'}),
                   processData:false,
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
            });
       });
</script>  

@endsection